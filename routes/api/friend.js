module.exports = function(ctrl, Joi) {
  return [{
  method : 'POST',
  path : '/api/users/sendFriendRequest',
  config : {
    auth: 'jwt',
    handler : ctrl.users.sendFriendRequest,
    description: 'Send friend request to facebook friend',
    tags: ['api'],
    validate: {
      payload:{
        userId : Joi.string()
      },
      headers: Joi.object({
            'authorization': Joi.string().required()
      }).unknown()
    },
    plugins: {
      'hapi-swagger': {
        responseMessages: [
        { code: 201, message: 'Created' },
        { code: 400, message: 'Bad Request' },
        { code: 500, message: 'Internal Server Error'}
        ]
      }
    }
  }
},
{
  method : 'PUT',
  path : '/api/users/acceptRejectDeleteFriend',
  config : {
    auth: 'jwt',
    handler : ctrl.users.acceptRejectDeleteFriend,
    description: 'Accept / Reject friend request',
    tags: ['api'],
    validate: {
      payload:{
        userId : Joi.string(),
        isAccept : Joi.string()
      },
      headers: Joi.object({
            'authorization': Joi.string().required()
      }).unknown()
    },
    plugins: {
      'hapi-swagger': {
        responseMessages: [
        { code: 201, message: 'Created' },
        { code: 400, message: 'Bad Request' },
        { code: 500, message: 'Internal Server Error'}
        ]
      }
    }
  }
},
{
  method : 'PUT',
  path : '/api/users/unFriendUser',
  config : {
    auth: 'jwt',
    handler : ctrl.users.unFriendUser,
    description: 'Unfriend the existing friend',
    tags: ['api'],
    validate: {
      payload:{
        userId : Joi.string()
      },
      headers: Joi.object({
            'authorization': Joi.string().required()
      }).unknown()
    },
    plugins: {
      'hapi-swagger': {
        responseMessages: [
        { code: 201, message: 'Created' },
        { code: 400, message: 'Bad Request' },
        { code: 500, message: 'Internal Server Error'}
        ]
      }
    }
  }
},
{
  method : 'GET',
  path : '/api/users/facebookFriendList/{requestType}/{page}',
  config : {
    auth: 'jwt',
    handler : ctrl.users.facebookFriendList,
    description : 'Get list of friends and requests',
    tags : ['api'],
    validate: {
      params: {
        requestType: Joi.string().alphanum().required(),
        page: Joi.number()
      },
      query: {
              searchQuery: Joi.string().trim().min(0).allow('').allow(null)
      },
      headers: Joi.object({
             'authorization': Joi.string().required()
       }).unknown()
      },
      response: {
        options: {
          allowUnknown: true
        }
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 200, message: 'OK' },
          { code: 400, message: 'Bad Request' },
          { code: 404, message: 'Employee Not Found' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
},
{
  method : 'GET',
  path : '/api/users/getFriendListAllDropp/{requestType}/{page}',
  config : {
    auth: 'jwt',
    handler : ctrl.users.getFriendListAllDropp,
    description : 'Get list of friends and requests',
    tags : ['api'],
    validate: {
      params: {
        requestType: Joi.string().alphanum().required(),
        page: Joi.number()
      },
      query: {
              searchQuery: Joi.string().trim().min(0).allow('').allow(null)
      },
      headers: Joi.object({
             'authorization': Joi.string().required()
       }).unknown()
      },
      response: {
        options: {
          allowUnknown: true
        }
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 200, message: 'OK' },
          { code: 400, message: 'Bad Request' },
          { code: 404, message: 'Employee Not Found' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
}

];
}

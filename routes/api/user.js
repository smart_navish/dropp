module.exports = function(ctrl, Joi) {
  return [{
    method : 'POST',
    path : '/api/users/fbLogin',
    config : {
      handler : ctrl.users.fbLogin,
      description: 'Validate & fbLogin',
      tags: ['api'],
      validate: {
        payload:{
          firstName : Joi.string().trim().min(0).allow('').max(100),
          lastName : Joi.string().trim().min(0).allow('').max(100),
          fbId : Joi.string(),
          fbAccToken : Joi.string(),
          email: Joi.string().email().trim().min(0).allow('').allow(null),
          gender: Joi.string().valid('female', 'male'),
          profilePic :  Joi.string().trim().min(0).allow('').allow(null),
          deviceType:[Joi.string().min(0).allow('').allow(null), Joi.number().min(0).allow('').allow(null)],
          deviceToken: Joi.string().trim().min(0).allow('').allow(null)
        }
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 201, message: 'Created' },
          { code: 400, message: 'Bad Request' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
  },
  {
    method : 'PUT',
    path : '/api/users/updateProfile',
    config : {
      auth: 'jwt',
      handler : ctrl.users.updateProfile,
      description: 'Validate & update profile',
      tags: ['api'],
      validate: {
        payload:{
          firstName : Joi.string().trim().min(0).allow('').max(100),
          lastName : Joi.string().trim().min(0).allow('').max(100),
          email: Joi.string().email().trim().min(0).allow('').allow(null),
          bio: Joi.string().trim().min(0).allow('').allow(null),
          gender: Joi.string().valid('female', 'male'),
          phoneNumber: {
            countryCode: Joi.string().trim().min(0).allow(''),
            countryObj: Joi.string().trim().min(0).allow(''),
            phone: Joi.string().trim().allow('')
          }
        },
        headers: Joi.object({
              'authorization': Joi.string().required()
        }).unknown()
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 201, message: 'Created' },
          { code: 400, message: 'Bad Request' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
  },
  {
    method : 'PUT',
    path : '/api/users/updateProfilePic',
    config : {
      auth: 'jwt',
      handler : ctrl.users.updateProfilePic,
      description: 'Validate & update profile',
      tags: ['api'],
      validate: {
        payload:{
          profilePic :  Joi.string().trim().min(3).max(100).required()
        },
        headers: Joi.object({
              'authorization': Joi.string().required()
        }).unknown()
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 201, message: 'Created' },
          { code: 400, message: 'Bad Request' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
  },
  {
    method : 'PUT',
    path : '/api/users/updateToken',
    config : {
      auth: 'jwt',
      handler : ctrl.users.updateToken,
      description: 'Validate & update token of user',
      tags: ['api'],
      validate: {
        payload:{
          deviceToken : Joi.string().trim().min(3).max(300).required(),
          isLogout : Joi.boolean()
        },
        headers: Joi.object({
              'authorization': Joi.string().required()
        }).unknown()
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 201, message: 'Created' },
          { code: 400, message: 'Bad Request' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
  },
  {
    method : 'GET',
    path : '/api/users/userdetails',
    config : {
      auth: 'jwt',
      handler : ctrl.users.userDetails,
      description : 'To find detail of user',
      tags : ['api'],
      validate: {
         headers: Joi.object({
               'authorization': Joi.string().required()
         }).unknown()
        },
        response: {
          options: {
            allowUnknown: true
          }
        },
        plugins: {
          'hapi-swagger': {
            responseMessages: [
            { code: 200, message: 'OK' },
            { code: 400, message: 'Bad Request' },
            { code: 404, message: 'Employee Not Found' },
            { code: 500, message: 'Internal Server Error'}
            ]
          }
        }
      }
  },
  {
    method : 'GET',
    path : '/api/users/notificationsList/{page}',
    config : {
      auth: 'jwt',
      handler : ctrl.users.notificationsList,
      description : 'To find notifications list',
      tags : ['api'],
      validate: {
          params: {
            page: Joi.number()
          },
         headers: Joi.object({
               'authorization': Joi.string().required()
         }).unknown()
        },
        response: {
          options: {
            allowUnknown: true
          }
        },
        plugins: {
          'hapi-swagger': {
            responseMessages: [
            { code: 200, message: 'OK' },
            { code: 400, message: 'Bad Request' },
            { code: 404, message: 'Employee Not Found' },
            { code: 500, message: 'Internal Server Error'}
            ]
          }
        }
      }
  },
  {
    method : 'GET',
    path : '/api/users/getUserById/{userId}',
    config : {
      auth: 'jwt',
      handler : ctrl.users.userDetails,
      description : 'To find detail of user',
      tags : ['api'],
      validate: {
        params: {
          userId: Joi.string().alphanum().required()
        },
        headers: Joi.object({
               'authorization': Joi.string().required()
         }).unknown()
        },
        response: {
          options: {
            allowUnknown: true
          }
        },
        plugins: {
          'hapi-swagger': {
            responseMessages: [
            { code: 200, message: 'OK' },
            { code: 400, message: 'Bad Request' },
            { code: 404, message: 'Employee Not Found' },
            { code: 500, message: 'Internal Server Error'}
            ]
          }
        }
      }
  },
  {
    method : 'PUT',
    path : '/api/users/deleteUserAccount',
    config : {
      auth: 'jwt',
      handler : ctrl.users.updateToken,
      description: 'Delete the user account',
      tags: ['api'],
      validate: {
        headers: Joi.object({
              'authorization': Joi.string().required()
        }).unknown()
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 201, message: 'Created' },
          { code: 400, message: 'Bad Request' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
  }
];
}

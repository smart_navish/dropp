module.exports = function(ctrl, Joi) {
  return [{
  method : 'POST',
  path : '/api/challenges/createChallenge',
  config : {
    auth: 'jwt',
    handler : ctrl.challenges.createChallenge,
    description: 'Create a new challenge by user',
    tags: ['api'],
    validate: {
      payload:{
        videoLink : Joi.string(),
        description : Joi.string(),
        endDateTime : Joi.number(),
        tagUsers : Joi.array(),
        hashTag : Joi.array()
      },
      headers: Joi.object({
            'authorization': Joi.string().required()
      }).unknown()
    },
    plugins: {
      'hapi-swagger': {
        responseMessages: [
        { code: 201, message: 'Created' },
        { code: 400, message: 'Bad Request' },
        { code: 500, message: 'Internal Server Error'}
        ]
      }
    }
  }
},
{
  method : 'PUT',
  path : '/api/challenges/acceptRejectChallenge',
  config : {
    auth: 'jwt',
    handler : ctrl.challenges.acceptRejectChallenge,
    description: 'Accept / Reject challenges',
    tags: ['api'],
    validate: {
      payload:{
        challengeId: Joi.string(),
        isAccepted : Joi.number()
      },
      headers: Joi.object({
            'authorization': Joi.string().required()
      }).unknown()
    },
    plugins: {
      'hapi-swagger': {
        responseMessages: [
        { code: 201, message: 'Created' },
        { code: 400, message: 'Bad Request' },
        { code: 500, message: 'Internal Server Error'}
        ]
      }
    }
  }
},
{
  method : 'PUT',
  path : '/api/challenges/addMoreTagUsers',
  config : {
    auth: 'jwt',
    handler : ctrl.challenges.addMoreTagUsers,
    description: 'Add More tagged user',
    tags: ['api'],
    validate: {
      payload:{
        challengeId: Joi.string(),
        tagUsers : Joi.array()
      },
      headers: Joi.object({
            'authorization': Joi.string().required()
      }).unknown()
    },
    plugins: {
      'hapi-swagger': {
        responseMessages: [
        { code: 201, message: 'Created' },
        { code: 400, message: 'Bad Request' },
        { code: 500, message: 'Internal Server Error'}
        ]
      }
    }
  }
},
{
method : 'POST',
path : '/api/challenges/commentOnChallenge',
config : {
  auth: 'jwt',
  handler : ctrl.challenges.commentOnChallenge,
  description: 'Comment on a challenge',
  tags: ['api'],
  validate: {
    payload:{
      //commentMessage : Joi.string(),
      //userTagged : Joi.array(),
      videoLink : Joi.string().trim().min(0).allow('').allow(null),
      challengeId : Joi.string()
    },
    headers: Joi.object({
          'authorization': Joi.string().required()
    }).unknown()
  },
  plugins: {
    'hapi-swagger': {
      responseMessages: [
      { code: 201, message: 'Created' },
      { code: 400, message: 'Bad Request' },
      { code: 500, message: 'Internal Server Error'}
      ]
    }
  }
}
},
{
  method : 'GET',
  path : '/api/challenges/commentListOfChallenge/{challengeId}',
  config : {
    auth: 'jwt',
    handler : ctrl.challenges.commentListOfChallenge,
    description : 'Get detail of challenge',
    tags : ['api'],
    validate: {
      params: {
        challengeId: Joi.string()
      },
      headers: Joi.object({
             'authorization': Joi.string().required()
       }).unknown()
      },
      response: {
        options: {
          allowUnknown: true
        }
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 200, message: 'OK' },
          { code: 400, message: 'Bad Request' },
          { code: 404, message: 'Employee Not Found' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
},
{
  method : 'GET',
  path : '/api/challenges/challengeDetail/{challengeId}',
  config : {
    auth: 'jwt',
    handler : ctrl.challenges.challengeDetail,
    description : 'Get detail of challenge',
    tags : ['api'],
    validate: {
      params: {
        challengeId: Joi.string()
      },
      headers: Joi.object({
             'authorization': Joi.string().required()
       }).unknown()
      },
      response: {
        options: {
          allowUnknown: true
        }
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 200, message: 'OK' },
          { code: 400, message: 'Bad Request' },
          { code: 404, message: 'Employee Not Found' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
},
{
  method : 'GET',
  path : '/api/challenges/getListOfChallenge/{type}/{page}/{activeCountValue}/{searchType}',
  config : {
    auth: 'jwt',
    handler : ctrl.challenges.getListOfChallenge,
    description : 'Get list of challenge and challenges can be, -- myCreatedChallenges, myAcceptedChallenges, myTagedChallenges, allChallenges, globalChallenges (And searchType - [hashTag or normal])',
    tags : ['api'],
    validate: {
      params: {
        type: Joi.string(),
        searchType : Joi.string(),
        page: Joi.number(),
        activeCountValue : Joi.number()
      },
      query: {
              userId: Joi.string().trim().min(0).allow('').allow(null),
              searchQuery: Joi.string().trim().min(0).allow('').allow(null)
      },
      headers: Joi.object({
             'authorization': Joi.string().required()
       }).unknown()
      },
      response: {
        options: {
          allowUnknown: true
        }
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 200, message: 'OK' },
          { code: 400, message: 'Bad Request' },
          { code: 404, message: 'Employee Not Found' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
},
{
  method : 'GET',
  path : '/api/challenges/getHashTags/{page}',
  config : {
    auth: 'jwt',
    handler : ctrl.challenges.getHashTags,
    description : 'Get list of hashTags',
    tags : ['api'],
    validate: {
      params: {
        page: Joi.number()
      },
      query: {
              searchQuery: Joi.string().trim().min(0).allow('').allow(null)
      },
      headers: Joi.object({
             'authorization': Joi.string().required()
       }).unknown()
      },
      response: {
        options: {
          allowUnknown: true
        }
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 200, message: 'OK' },
          { code: 400, message: 'Bad Request' },
          { code: 404, message: 'Employee Not Found' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
},
{
  method : 'GET',
  path : '/api/challenges/ffmpeg',
  config : {
    handler : ctrl.challenges.ffmpeg,
    description : 'testing',
    tags : ['api'],
      response: {
        options: {
          allowUnknown: true
        }
      },
      plugins: {
        'hapi-swagger': {
          responseMessages: [
          { code: 200, message: 'OK' },
          { code: 400, message: 'Bad Request' },
          { code: 404, message: 'Employee Not Found' },
          { code: 500, message: 'Internal Server Error'}
          ]
        }
      }
    }
}
];
}

'use strict';
var Joi = require('joi');
var ctrl = require('./../controllers');
var user = require('./api/user')(ctrl, Joi);
var friend = require('./api/friend')(ctrl, Joi);
var challenge = require('./api/challenge')(ctrl, Joi);
var allRoutes = [];


var apiRoutes = allRoutes.concat(user,friend,challenge);
module.exports = apiRoutes;

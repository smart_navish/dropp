var people = { // our "users database"
    1: {
      id: 1,
      name: 'Jen Jones'
    }
};

// bring your own validation function
var validate = function (decoded, request, callback) {

    // do your checks to see if the person is valid
    if (!people[decoded.id]) {
      return callback(null, false);
    }
    else {
      return callback(null, true);
    }
};
exports.register = function(server, options, next) {
  //-- server.register has a callback, which might better to put server.auth.strategy on it
  server.register(require('hapi-auth-jwt2'), (err) => {
    if (err) { console.log('(X) hapi-auth-jwt-2 registration failed!'); }
    server.auth.strategy('jwt', 'jwt',
    { key: 'NeverShareYourSecret',          // Never Share your secret key
      validateFunc: validate,            // validate function defined above
      verifyOptions: { algorithms: [ 'HS256' ] } // pick a strong algorithm
    });

    //server.auth.default('jwt');
    next();
  });
}

exports.register.attributes = {
  name: 'auth',
  version: '1.0.0',
  once: true
}

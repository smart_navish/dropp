var friends = require('./../models').friends;
var Common = require('./../config/common');
/*insert query*/
exports.createFriend = function(objectToSave, callback) {
    friends.create(objectToSave, callback);
}
exports.findFriends = function(type, findObj, callback){
    if(type === 1){
      friends.findOne(findObj, callback);
    }else {
      friends.find(findObj, callback)
    }
}
exports.friendCount = function(matchObj) {
  return new Promise(function(resolve, reject) {
    friends.count(matchObj, function(err, counts) {
      if (err) {
          reject(err);
      } else {
          resolve(counts);
      }
    });
  })
}

exports.findFriendExec = function(type, findObj, populate, callback){if(type === 1){
      friends.findOne(findObj)
      .populate('senderId')
      .exec(callback);
    }else {
      friends.find(findObj)
      .populate('senderId')
      .exec(callback);
    }
}

exports.updateFriend = function(matchObj, updateObj, options, callback) {
    friends.findOneAndUpdate(matchObj, { $set: updateObj }, options, callback);
}
exports.removeFriend = function(matchObj, callback) {
    //friends.remove(matchObj, callback);
    friends.findOneAndUpdate(matchObj, { $set: {relationStatus: 0} }, {new:true}, callback);
}
exports.friendIds = function(userId) {
  return new Promise(function(resolve, reject) {
    var friendsdata = [];
    var condition = {
      $or : [{senderId : Common.toObjectId(userId), relationStatus: 3}, {receiverId : Common.toObjectId(userId), relationStatus: 3}]
    }
    friends.find(condition, function(err, response) {
        if (response) {
          for (var i = 0; i < response.length; i++) {
              if(userId == response[i].senderId){
                  friendsdata.push(Common.toObjectId(response[i].receiverId));
              }else {
                  friendsdata.push(Common.toObjectId(response[i].senderId));
              }
          }
        }
        resolve(friendsdata);
    });
  });
}

exports.aggreateQuery = function(matchObj, page, searchText, userId, callback) {
  friends.aggregate([
        {
           $match :matchObj
        },
        {
           $lookup : {
               from : 'users',
               localField : 'senderId',
               foreignField : '_id',
               as: 'senderId'
           }
        },
        {
          $unwind: { "path": "$senderId", "preserveNullAndEmptyArrays": true }
        },
        {
           $lookup : {
               from : 'users',
               localField : 'receiverId',
               foreignField : '_id',
               as: 'receiverId'
           }
        },
        {
          $unwind: { "path": "$receiverId", "preserveNullAndEmptyArrays": true }
        },
        {
          $project: {
              senderId : 1,
              receiverId : 1,
              relationStatus : 1,
              source : 1,
              'fullName' : {
                $cond: [{$eq: [ "$senderId._id", userId ]}, "$receiverId.fullName", "$senderId.fullName"]
              }
            }
        },
        {
          $match :{
            "fullName" : {$regex: searchText, '$options': 'i'}
          }
        },
        {
                $skip: page * 10
        },
        {
                $limit: 10
        }
    ]).exec(callback)
}


exports.db = friends;

var users = require('./../models').users;

/*insert query*/
exports.createuser = function(objectToSave, callback) {
    users.create(objectToSave, callback);
}
exports.findusers = function(type, findObj, callback){
    if(type === 1){
      users.findOne(findObj, callback);
    }else {
      users.find(findObj, callback)
    }
}

exports.finduserspage = function(findObj, page, callback){
    users.find(findObj).skip(page*10).limit(10).exec(callback);
}
exports.findUserExec = function(type, findObj, populate, callback){
    if(type === 1){
      users.findOne(findObj).populate(populate).exec(callback);
    }else {
      users.find(findObj).populate(populate).exec(callback);
    }
}
exports.userCount = function(matchObj) {
  return new Promise(function(resolve, reject) {
    users.count(matchObj, function(err, counts) {
      if (err) {
          resolve(0);
      } else {
          resolve(counts);
      }
    });
  });
}
exports.updateUser = function(matchObj, updateObj, options, callback) {
    users.findOneAndUpdate(matchObj, { $set: updateObj }, options, callback);
}

var challenges = require('./../models').challenges;
var hashtags = require('./../models').hashtags;
/*insert query*/
exports.createChallenge = function(objectToSave, callback) {
    challenges.create(objectToSave, callback);
}
exports.findChallenges = function(type, findObj, callback){
    if(type === 1){
      challenges.findOne(findObj, callback);
    }else {
      challenges.find(findObj, callback)
    }
}

exports.getDifferentTagUser = function(findObj, tagged) {
  return new Promise(function(resolve, reject) {
    challenges.findOne(findObj, function(err, result) {
      if(result){
        var arr1 = result.tagUsers;
        var arr2 = tagged;
        var result = arr1.map(a => String(a.userId));
        var finalArray = arr2.filter(function(e) {
           return result.indexOf(e) === -1
        });
        resolve(finalArray);
      }else {
        resolve(null);
      }
    })
  });

}



exports.challengeCount = function(matchObjData, searchText, searchType) {
  console.log(matchObjData, searchText, searchType);
  return new Promise(function(resolve, reject) {
    if(searchType == 'hashTag'){
      matchObjData.hashTag ={"$elemMatch":{$regex: searchText, '$options': 'i'}};
    }else {
      matchObjData.description = {$regex: searchText, '$options': 'i'};
    }
   // console.log('******', matchObjData);
    challenges.count(matchObjData, function(err, counts) {
      if (err) {
          reject(err);
      } else {
          resolve(counts);
      }
    });
  })
}

exports.updateChallenge = function(matchObj, updateObj, options, callback) {
    challenges.findOneAndUpdate(matchObj, { $set: updateObj }, options, callback);
}

exports.addToSet = function(matchObj, addToSetQuery, options, callback) {
    challenges.findOneAndUpdate(matchObj, { $addToSet: addToSetQuery }, options, callback);
}

exports.removeChallenge = function(matchObj, callback) {
    challenges.remove(matchObj, callback);
}

exports.saveHash = function(hashTag) {
  return new Promise(function(resolve, reject) {
    hashtags.findOneAndUpdate({hashName : hashTag}, { "$inc": {totalChallenges: 1} }, {upsert : true}, function (err, result) {
        console.log(err, result);
        if (result) {
          resolve(result);
        }
        resolve(null);
    });
  });
}

exports.getHashTags = function(searchText, page) {
  return new Promise(function(resolve, reject) {
    if(typeof(page) == 'number'){
      hashtags.find({'hashName':{$regex: searchText, '$options': 'i'}}).skip(page*10).limit(10).exec(function(err, result) {
        if(result){
            resolve(result);
        }
        resolve([]);
      });
    }else {
      hashtags.count({'hashName':{$regex: searchText, '$options': 'i'}}).exec(function(err, counts) {
        if(counts){
            resolve(counts);
        }
        resolve(0);
      });
    }

  });

}

exports.aggreateQuery = function(matchObj, searchText, page, searchType, typeoffetch, callback) {
  var queryToMatch = [
        {
           $match :matchObj
        },
        {
         $lookup : {
             from : 'users',
             localField : 'userId',
             foreignField : '_id',
             as: 'user'
         }
        },
        {
            $unwind: { "path": "$user", "preserveNullAndEmptyArrays": true }
        },
        {
            $project: {
                    tagUsers : 1,
                    hashTag: 1,
                    comments : 1,
                    createdAt: 1,
                    user: 1,
                    videoLink:1,
                    description:1,
                    endDateTime: 1,
                    dayDiff: {
                        $subtract:[
                            "$endDateTime", new Date()
                        ]
                    }
            }
        },


    ];
  if(typeoffetch == 'expired'){
    queryToMatch.push({
            $sort: {
                 "dayDiff": -1
            }
    });
  }else {
    queryToMatch.push({
            $sort: {
                 "dayDiff":  1
            }
    });
  }

  if(searchType == 'hashTag'){
    queryToMatch.push({
      $match :{
        "hashTag" : {$regex: searchText, '$options': 'i'}
      }
    });
  }else {
    queryToMatch.push({
      $match :{
        "description" : {$regex: searchText, '$options': 'i'}
      }
    });
  }
  queryToMatch.push({
          $skip: page * 10
  },
  {
          $limit: 10
  });
  console.log('------------',JSON.stringify(queryToMatch));
  challenges.aggregate(queryToMatch).exec(callback);
}


exports.db = challenges;

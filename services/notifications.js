var notifications = require('./../models').notifications;
var users = require('./../models').users;
var fcm = require('fcm-notification');
var FCM = new fcm(__dirname+'/dropp-app1-firebase-adminsdk-rnt6w-eed7d57e66.json');

exports.sendPush = function(type, userObj, notificationBody = 'null'){
  return new Promise(function(resolve, reject) {
    var dataToSave = {
      notificationType : type,
      senderId : userObj.senderId,
      receiverId : userObj.receiverId,
      createdAt : new Date(),
      challengeId : userObj.challengeId ? userObj.challengeId : null
    }
    users.findOne({'_id': userObj.senderId},function(err, userData) {
        if(userData){
            if(type == 1){
                  delete dataToSave.challengeId;
                  dataToSave.notificationBody = userData.firstName + ' send you a friend request';
            }
            if(type == 2){
                  delete dataToSave.challengeId;
                  dataToSave.notificationBody = userData.firstName + ' accepted your friend request';
            }
            else if (type == 3) {
                  dataToSave.notificationBody = userData.firstName + ' tagged you in challenge';
            }
            else if (type == 4) {
                  dataToSave.notificationBody = userData.firstName + ' commented on your challenge';
            }
            else if (type == 5) {
                  dataToSave.notificationBody = userData.firstName + ' accept your challenge';
            }
            else if (type == 6) {
                  dataToSave.notificationBody = userData.firstName + ' tagged you in challenge comment';
            }
        }
        notifications.create(dataToSave, function(error, notiData) {
            if(!error){

                var topic = 'Dropp_'+userObj.receiverId;
                var notificationInfo = {
                      "notificationType": dataToSave.notificationType,
                      "senderId":dataToSave.senderId,
                      "receiverId":dataToSave.receiverId,
                      "challengeId":dataToSave.challengeId,
                      "notificationBody":dataToSave.notificationBody
                };
                console.log('+++++++++++++++++++++++'+topic, notificationInfo);
                notificationInfo.notificationId = notiData._id;
                var message = {
                      data: {    //This is only optional, you can send any data
                          'notificationInfo' : JSON.stringify(notificationInfo)
                      },
                      notification:{
                          title : 'Dropp',
                          body : dataToSave.notificationBody
                      },
                      topic : topic
                    };
                console.log('message', message)
                FCM.send(message, function(err, response) {
                    if(err){
                        reject(err);
                    }else {
                        resolve(response);
                    }
                })
            }
        });
    })
  });
}


exports.subscribeToPush = function(tokens, TopicName) {
  return new Promise(function(resolve, reject) {
    console.log(tokens, TopicName);
    FCM.subscribeToTopic(tokens, TopicName, function(err, response) {
      if(err){
          reject(err);
      }else {
          resolve(response);
      }
    })
  });
}

exports.unsubscribeFromPush = function(tokens, TopicName) {
  return new Promise(function(resolve, reject) {
    console.log(tokens, TopicName);
    FCM.unsubscribeFromTopic(tokens, TopicName, function(err, response) {
      if(err){
          reject(err);
      }else {
          resolve(response);
      }
    })
  });
}

/*insert query*/
exports.createNoti = function(objectToSave, callback) {
    notifications.create(objectToSave, callback);
}
exports.findNotiExec = function(type, findObj, page, callback){
    findObj.isRead = false;
    console.log('findObj', findObj);
    if(type === 1){
      notifications.findOne(findObj).populate('senderId').populate('receiverId').exec(callback);
    }else {
      console.log(page);
      notifications.find(findObj).sort({'createdAt':-1}).skip(page*20).limit(20).populate('senderId').populate('receiverId').exec(callback);
    }
}
exports.notiCount = function(matchObj) {
  return new Promise(function(resolve, reject) {
    matchObj.isRead = true;
    notifications.count(matchObj, function(err, counts) {
      if (err) {
        reject(err);
      } else {
        resolve(counts);
      }
    });
  });
}
  exports.readNotification = function(notificationType, receiverId, challengeId){
    return new Promise(function(resolve, reject) {
      if(notificationType === 3 || notificationType === 4){
          var queryForUpdate = {challengeId : challengeId, receiverId : receiverId};
      }else {
          var queryForUpdate = {challengeId : null, receiverId : receiverId};
      }
      notifications.updateMany(queryForUpdate, {$set : {"isRead": true}}).exec(async function(err, result) {
          console.log(err, result);
          resolve(true);
      });
    });
  }

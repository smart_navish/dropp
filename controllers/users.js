'use-strict'
var Users = require('./../services/users');
var Noti = require('./../services/notifications');
var Friends = require('./../services/friends');
var Boom = require('boom');
var Common = require('./../config/common');
var Emails = require('./../config/emails');
var FbGraph = require('fbgraph');
/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 23.02.2018 / Navish
/****************************************************/

/* fbLogin of end user 23.02.2018 By Navish */

exports.fbLogin = function (request, reply) {
    console.log('In fb_login');
    var payloadData = request.payload;
    var userInfo = {};
    userInfo.email = (payloadData.email != '' ? payloadData.email : '');
    userInfo.firstName = (payloadData.firstName != '' ? payloadData.firstName : '');
    userInfo.lastName = (payloadData.lastName != '' ? payloadData.lastName : '');
    userInfo.fullName = userInfo.firstName+" "+userInfo.lastName;
    userInfo.phoneNumber = (payloadData.phoneNumber != '' ? payloadData.phoneNumber : '');
    userInfo.fbId = payloadData.fbId;
    userInfo.fbAccToken = payloadData.fbAccToken;
    userInfo.profilePic = payloadData.profilePic;
    userInfo.gender = payloadData.gender;
    userInfo.verified = true;
    userInfo.deviceType = payloadData.deviceType;
    userInfo.deviceToken = payloadData.deviceToken;
    var deviceToken = new Array(payloadData.deviceToken);
    //languages.create({name: "abc"});

    console.log('body-------', JSON.stringify(payloadData) + 'and login', userInfo.fbId+ ' and deviceToken', deviceToken);
    Users.findusers(1, {'fbId': userInfo.fbId},function(err, userObj) {
        if(!err){
            console.log('userExist and count is ', userObj);
            if(userObj)
            {
              Users.updateUser({'_id': userObj._id},{fbAccToken : userInfo.fbAccToken, isDeleted : false}, {new: true}, function(err, userObj) {
                  if(!err){
                    var tokenData = {
                        name: userObj.name,
                        id: userObj._id,
                        fbAccToken: userObj.fbAccToken
                    };
                    userObj.token = Common.getToken(tokenData)
                    userObj.alreadyExist = true;
                    userObj = Common.customeResponse(['createdAt', 'updatedAt', 'phoneNumber','__v'], userObj)
                    reply(Common.successResponse('Successfully authenticated!', userObj));
                  }
                  else
                  {
                      return reply(Boom.badImplementation(err));
                  }
              });
            }
            else
            {
                Users.createuser(userInfo, function (err, userObj) {
                    if(!err)
                    {
                      var tokenData = {
                          name: userObj.name,
                          id: userObj._id,
                          fbAccToken: userObj.fbAccToken
                      };
                      userObj.token = Common.getToken(tokenData)
                      userObj.alreadyExist = false;
                      userObj = Common.customeResponse(['createdAt', 'updatedAt', 'phoneNumber','__v'], userObj)
                      reply(Common.successResponse('Successfully authenticated!', userObj));
                    }
                    else
                    {
                        return reply(Boom.badImplementation(err));
                    }
                });
            }
        }
        else
        {
            return reply(Boom.badImplementation(err));
        }
    });
};
/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 23.02.2018 / Navish
/****************************************************/

/* Update detail of end user 23.02.2018 By Navish */
exports.userDetails = function(request, reply) {
  console.log('userId-- ', Common.getUserId(request.headers.authorization), request.params)
  var userId = Common.getUserId(request.headers.authorization);
  if(request.params.userId){
      console.log('request.params.userId', request.params.userId)
      var query = {_id : request.params.userId};
  }else {
      console.log('request.headers.authorization', Common.getUserId(request.headers.authorization))
      var query = {_id : Common.getUserId(request.headers.authorization)};
  }
  Users.findusers(null, query, function(err, userObj){
    if(err){
      return reply(Boom.badImplementation(err));
    } else if(userObj.length == 0){
      return reply(Boom.notFound('User not found'));
    } else {
      //var userObj = userObj.toObject();
    if(request.params.userId){
      console.log('request.params.userId another', request.params.userId)
      checkRelationStatus(0, userId, userObj, [], function(mutualUser) {
         reply(Common.successResponse('Successfully fetched!', mutualUser[0]));
      });
    }else {

console.log('request.headers.authorization another', Common.getUserId(request.headers.authorization))
      userObj = Common.customeResponse(['createdAt', 'updatedAt', '__v'], userObj[0])
      reply(Common.successResponse('Successfully fetched!', userObj));
    }
    }
  });
}

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 26.02.2018 / Navish
/****************************************************/

/* Update profile of end user 26.02.2018 By Navish */
exports.updateProfile = function (request, reply) {
    var userId = Common.getUserId(request.headers.authorization);
    var payloadData = request.payload;
    var userInfo = {};
    if(payloadData.email != ''){
      userInfo.email = payloadData.email;
    }
    userInfo.firstName = (payloadData.firstName != '' ? payloadData.firstName : '');
    userInfo.lastName = (payloadData.lastName != '' ? payloadData.lastName : '');
    userInfo.phoneNumber = (payloadData.phoneNumber != '' ? payloadData.phoneNumber : '');
    userInfo.gender = payloadData.gender;
    userInfo.bio = payloadData.bio;
    //languages.create({name: "abc"});
    Users.updateUser({'_id': userId},userInfo, {new: true}, function(err, userObj) {
        if(!err){
            userObj = Common.customeResponse(['createdAt', 'updatedAt', '__v'], userObj)
            reply(Common.successResponse('Profile updated successfully.', userObj));
        }
        else
        {
            return reply(Boom.badImplementation(err));
        }
    });
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 26.02.2018 / Navish
/****************************************************/

/* Update profile pic of end user 26.02.2018 By Navish */
exports.updateProfilePic = function (request, reply) {
    var userId = Common.getUserId(request.headers.authorization);
    var payloadData = request.payload;
    var userInfo = {};
    userInfo.profilePic = payloadData.profilePic;
    Users.updateUser({'_id': userId},userInfo, {new: true}, function(err, userObj) {
        if(!err){
            userObj = Common.customeResponse(['createdAt', 'updatedAt', '__v'], userObj)
            reply(Common.successResponse('Profile picture updated successfully!', userObj));
        }
        else
        {
            return reply(Boom.badImplementation(err));
        }
    });
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 05.03.2018 / Navish
// Not in use for phase 2 *****************
/****************************************************/

/* Get Send Friend request for end user 05.03.2018 By Navish */
exports.sendFriendRequest = async function (request, reply) {
    try {
      var userId = Common.getUserId(request.headers.authorization);
      var payloadData = request.payload;
      var friendUserId = payloadData.userId;
      var condition = {$or : [{senderId : userId, receiverId : friendUserId}, {receiverId : userId, senderId : friendUserId}]};
      var userExist = await Friends.friendCount(condition);
      if(userExist > 0) return reply(Common.successResponse('You have already send request or receive request with this user!', userExist));

      var dataToEnter = {
        senderId : userId,
        receiverId : friendUserId,
        relationStatus : 1
      }
      Friends.createFriend(dataToEnter, async function(err, friendObj) {
          if(!err){
              var notiresponse = await Noti.sendPush(1, dataToEnter);
              console.log(notiresponse);
              friendObj = Common.customeResponse(['createdAt', 'updatedAt', '__v', 'relationStatus'], friendObj)
              reply(Common.successResponse('Successfully send friend request!', friendObj));
          }
          else
          {
              return reply(Boom.badImplementation(err));
          }
      });
    } catch (e) {
        return reply(Boom.badImplementation(e));
    }
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 05.03.2018 / Navish
// Not in use for phase 2 *****************
/****************************************************/

/* Accept/Reject friend request for end user 05.03.2018 By Navish */
exports.acceptRejectDeleteFriend = function (request, reply) {
    try {
      var userId = Common.getUserId(request.headers.authorization);
      var payloadData = request.payload;
      var friendUserId = payloadData.userId;
      var isAccept = payloadData.isAccept;
      var condition = {$or : [{senderId : userId, receiverId : friendUserId}, {receiverId : userId, senderId : friendUserId}]};
      Friends.findFriends(1, condition, function(err, friendRes) {
          if(!err){
              if(friendRes){
                var updateCondition = {receiverId : userId, senderId : friendUserId};
                if(friendRes.senderId == userId) reply(Common.successResponse('You can not accept/reject this user!', friendRes));
                if(isAccept == "true"){
                    Friends.updateFriend(updateCondition, {relationStatus : 3}, {new: true}, async function(err, updateRes) {
                        if(!err){
                          var notiresponse = await Noti.sendPush(2, {receiverId : friendUserId, senderId : userId});
                          console.log(notiresponse);
                          return reply(Common.successResponse('Request has been accepted!', updateRes));
                        }
                        else {
                          return reply(Boom.badImplementation(err));
                        }
                    })
                }else {
                    Friends.removeFriend(updateCondition, function(err, removeObj) {
                        if(!err) return reply(Common.successResponse('Request has been rejected!', 'deleted'));
                        else {
                          return reply(Boom.badImplementation(err));
                        }
                    })
                }
              }else {
                reply(Common.successResponse('You can not accept/reject this user, user not exist!', friendRes));
              }
          }else {
              return reply(Boom.badImplementation(err));
          }
      });
    } catch (e) {
        return reply(Boom.badImplementation(e));
    }
};


/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 07.03.2018 / Navish
/****************************************************/

/* Delete friend request for end user 07.03.2018 By Navish */
exports.unFriendUser = function (request, reply) {
    try {
      var userId = Common.getUserId(request.headers.authorization);
      var payloadData = request.payload;
      var friendUserId = payloadData.userId;
      var condition = {$or : [{senderId : userId, receiverId : friendUserId}, {receiverId : userId, senderId : friendUserId}]};
      Friends.findFriends(1, condition, function(err, friendRes) {
          if(!err){
              if(friendRes){
                  Friends.removeFriend(condition, function(err, removeObj) {
                      if(!err) return reply(Common.successResponse('User has been removed successfully!', {}));
                      else {
                        return reply(Boom.badImplementation(err));
                      }
                  })
              }else {
                reply(Common.successResponse('You can not unfriend this user, user not exist!', friendRes));
              }
          }else {
              return reply(Boom.badImplementation(err));
          }
      });
    } catch (e) {
        return reply(Boom.badImplementation(e));
    }
};


/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 05.03.2018 / Navish
// Not in use for phase 2 *****************
/****************************************************/

/* Get friend List for end user 05.03.2018 By Navish */
exports.facebookFriendList = async function (request, reply) {
  var userId = Common.getUserId(request.headers.authorization);
  var fbAccToken = Common.getFbToken(request.headers.authorization);
    var payloadData = request.params;
    var userInfo = {};
    //Type can be **”myFriend/friendRequest/findFriend**
    userInfo.requestType = payloadData.requestType;
    var searchText = request.query.searchQuery?request.query.searchQuery:"";
    var page = payloadData.page;
    var responseData = {};
    switch (userInfo.requestType) {
      case 'findFriend':
        var fbFriends = await findFriend(fbAccToken, []);
        var mutualUser = await findAndUpdateFriends(userId, fbFriends, page, searchText, []);
        responseData.totalListSize = mutualUser.length;
        responseData.friendList = mutualUser;
        reply(Common.successResponse('Successfully fetched!', responseData));
        break;
      case 'friendRequest':
          var mutualUser = await getFriendList('friendRequest', userId, page, searchText);
          var condition = {receiverId : Common.toObjectId(userId), relationStatus: 1};
          responseData.totalListSize = await Friends.friendCount(condition);
          responseData.friendList = mutualUser;
          reply(Common.successResponse('Successfully fetched!', responseData, searchText));
          break;
      case 'myFriend':
          var mutualUser = await getFriendList('myFriend', userId, page, searchText);
          var condition = {
            $or : [{senderId : Common.toObjectId(userId), relationStatus: 3}, {receiverId : Common.toObjectId(userId), relationStatus: 3}]
          };
          responseData.totalListSize = await Friends.friendCount(condition);
          responseData.friendList = mutualUser;
          reply(Common.successResponse('Successfully fetched!', responseData));
          break;
      default:
    }
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 30.07.2018 / Navish
/****************************************************/

/* Get friend List for end user 30.07.2018 By Navish */
exports.getFriendListAllDropp = async function (request, reply) {
  var userId = Common.getUserId(request.headers.authorization);
  var fbAccToken = Common.getFbToken(request.headers.authorization);
    var payloadData = request.params;
    var userInfo = {};
    //Type can be **”myFriend/friendRequest/findFriend**
    userInfo.requestType = payloadData.requestType;
    var searchText = request.query.searchQuery?request.query.searchQuery:"";
    var page = payloadData.page;
    var responseData = {};
    switch (userInfo.requestType) {
      case 'myFriend':
        var fbFriends = await findFriend(fbAccToken, []);
        var mutualUser = await findFbDroppInstalled(userId, fbFriends, page, []);
        var myFrnds = await getFriendList('myFriend', userId, page, searchText);
        var condition = {
          $or : [{senderId : Common.toObjectId(userId), relationStatus: 3}, {receiverId : Common.toObjectId(userId), relationStatus: 3}]
        };
        responseData.totalListSize = await Friends.friendCount(condition);
        responseData.friendList = myFrnds;
        reply(Common.successResponse('Successfully fetched!', responseData));
        break;
      case 'findFriend':
        var findFrnd = await findAllDroppInstalled(userId, page, searchText, []);
        var condition = {'fullName':{$regex: searchText, '$options': 'i'}};
        responseData.totalListSize = await Users.userCount(condition);
        responseData.friendList = findFrnd;
        reply(Common.successResponse('Successfully fetched!', responseData));
        break;
      default:
    }
};


/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 29.03.2018 / Navish
/****************************************************/

/* Update token and subscribe for notification  end user 29.03.2018 By Navish */
exports.updateToken = function (request, reply) {
  console.log('in update token................................')
    var userId = Common.getUserId(request.headers.authorization);
    var payloadData = request.payload;
    var userInfo = {};
    userInfo.deviceToken = payloadData.deviceToken;
    userInfo.isLogout = payloadData.isLogout;
    if(userInfo.isLogout == true){
      var dataToUpdate = {deviceToken : ""}
    }else {
      var dataToUpdate = {deviceToken : userInfo.deviceToken}
    }
    console.log('dataToUpdate', dataToUpdate + 'payloadData', payloadData+ 'userId', userId, userInfo.isLogout)
    Users.updateUser({'_id': userId},userInfo, {new: true}, async function(err, userObj) {
        if(!err){
            if(userInfo.isLogout == true){
              var notiresponse = await Noti.unsubscribeFromPush([userInfo.deviceToken], 'Dropp_'+userObj._id);
            }else {
              var notiresponse = await Noti.subscribeToPush([userInfo.deviceToken], 'Dropp_'+userObj._id);
            }
            console.log('User data', userObj, notiresponse);
            userObj = Common.customeResponse(['createdAt', 'updatedAt', '__v'], userObj)
            reply(Common.successResponse('Successfully updated and fetched!', userObj));
        }
        else
        {
            return reply(Boom.badImplementation(err));
        }
    });
};


/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 29.03.2018 / Navish
/****************************************************/

/* get notifications list end user 29.03.2018 By Navish */
exports.notificationsList = function (request, reply) {
    var userId = Common.getUserId(request.headers.authorization);
    var payloadData = request.params;
    console.log(payloadData);
    var page = payloadData.page;
    var notificationList = [];
    var findObj = {receiverId: userId};
    Noti.findNotiExec(null, findObj, page, async function(err, notiObj) {
        if(!err){
            var totalListSize = await Noti.notiCount(findObj);
            console.log('totalListSize', totalListSize);
            for (var i = 0; i < notiObj.length; i++) {
              var notificationObj = {};
              notificationObj.notificationId = notiObj[i]._id;
              notificationObj.notificationType = notiObj[i].notificationType;
              notificationObj.senderInfo = {"senderId": notiObj[i].senderId._id,"fullName": notiObj[i].senderId.fullName, "profilePic": notiObj[i].senderId.profilePic};
              notificationObj.receiverInfo = {"receiverId": notiObj[i].receiverId._id,"fullName": notiObj[i].receiverId.fullName, "profilePic": notiObj[i].receiverId.profilePic};
              notificationObj.challengeId = notiObj[i].challengeId;
              notificationObj.notificationBody = notiObj[i].notificationBody;
              notificationObj.createdAt = (notiObj[i].createdAt).getTime();
              notificationList.push(notificationObj);
            }
            var dataToSend = {
              totalListSize : totalListSize,
              notificationList :  notificationList
            }
            reply(Common.successResponse('Successfully updated and fetched!', dataToSend));
        }
        else
        {
            return reply(Boom.badImplementation(err));
        }
    });
};


/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 29.03.2018 / Navish
/****************************************************/

/* Delete user account 19.04.2018 By Navish */
exports.deleteUserAccount = function (request, reply) {
    var userId = Common.getUserId(request.headers.authorization);
    var userInfo = {
      isDeleted : true
    };
    Users.updateUser({'_id': userId},userInfo, {new: true}, async function(err, userObj) {
        if(!err){
            reply(Common.successResponse('You account has been successfully deleted!'));
        }
        else
        {
            return reply(Boom.badImplementation(err));
        }
    });
};


//Related to find friend apis 05/03/2018
var findFriend = async function(accessToken, friendsArray) {
  return new Promise(function(resolve) {
    FbGraph.setAccessToken(accessToken);
    console.log("accessTokenaccessTokenaccessTokenaccessToken.........");
      FbGraph.get("me/friends?limit=50", function(err, res) {
        console.log(accessToken+'err, res..........', err, res);
        if(!err){
          for (var i = 0; i < res.data.length; i++) {
            friendsArray.push(res.data[i].id);
            console.log(res.data[i].name);
          }
          if(res.paging && res.paging.next) {
            friendsArray = findNext(res.paging.next, friendsArray, function(friendsArray) {
              resolve(friendsArray);
            });
          }
          else {
            resolve(friendsArray);
          }
        }else {
          resolve(friendsArray);
        }
    });
  })
}
//if pagination 05/03/2018
var findNext = function(nextToken, friendsArray, callback) {
        FbGraph.get(nextToken, function(err, res) {
          if(!err){
                for (var i = 0; i < res.data.length; i++) {
                    friendsArray.push(res.data[i].id);
                    console.log(res.data[i].name);
                }
              if(res.paging && res.paging.next) {
                  console.log('Not here');
                  findNext(res.paging.next, friendsArray, callback);
                }else {
                  console.log('here', friendsArray);
                  callback(friendsArray);
                }
            }else {
              console.log('here er', friendsArray);
              callback(friendsArray);
            }
      });
}
//check facebook users exists in database or not 05/03/2018
var findAndUpdateFriends = async function(userId, fbFriends, page, searchText, mutualUser) {
  return new Promise(function(resolve) {
    Users.finduserspage({'fbId': {'$in' : fbFriends}, 'fullName':{$regex: searchText, '$options': 'i'}}, page, async function(err, userObj) {
      checkRelationStatus(0, userId, userObj, mutualUser, function(mutualUser) {
         resolve(mutualUser);
      });
    });
  })
}

//Find Facebook with dropp installed friendSchema
var findFbDroppInstalled = async function(userId, fbFriends, page, mutualUser) {
  return new Promise(function(resolve) {
    Users.finduserspage({'fbId': {'$in' : fbFriends}}, page, async function(err, userObj) {
      addFriendToMutualUser(0, userId, userObj, mutualUser, function(mutualUser) {
        console.log('mutualUser', mutualUser);
         resolve(mutualUser);
      });
    });
  })
}


//Find All users with dropp installed friendSchema
var findAllDroppInstalled = async function(userId, page, searchText, mutualUser) {
  return new Promise(function(resolve) {
    Users.finduserspage({'_id': {$ne : userId},'fullName':{$regex: searchText, '$options': 'i'}}, page, async function(err, userObj) {
      checkRelationStatus(0, userId, userObj, mutualUser, function(mutualUser) {
         resolve(mutualUser);
      });
    });
  })
}

//addFriendToMutualUser
var addFriendToMutualUser = async function(i, userId, userObj, mutualUserArray, cb) {
  console.log('userObj.length', i, userObj.length);
      if(i < userObj.length){
        console.log('--------In ');
        var condition = {$or : [{senderId : userId, receiverId : userObj[i]._id}, {receiverId : userId, senderId : userObj[i]._id}]};
        var userExist = await Friends.friendCount(condition);
        console.log('userExist', userExist);
        if(userExist == 0){
            var dataToEnter = {
              senderId : userId,
              receiverId : userObj[i]._id,
              relationStatus : 3,
              source : "facebook"
            }
            Friends.createFriend(dataToEnter, async function(err, friendObj) {
              mutualUserArray.push({
                "_id"           : userObj[i]._id,
                "fbId"          : userObj[i].fbId,
                "firstName"     : userObj[i].firstName,
                "lastName"      : userObj[i].lastName,
                "profilePic"    : userObj[i].profilePic,
                "email"         : userObj[i].email,
                "bio"           : (userObj[i].bio) ? userObj[i].bio : ""
              })
              i = i+1;
              addFriendToMutualUser(i, userId, userObj, mutualUserArray, cb);
            });
        }else {
          i = i+1;
            addFriendToMutualUser(i, userId, userObj, mutualUserArray, cb);
        }
      }else {
        cb(mutualUserArray)
      }
}

//check status with users friend or not friend 05/03/2018
var checkRelationStatus = function(i, userId, userObj, mutualUserArray, cb) {
  console.log('userObj.length', userObj.length);
      if(i < userObj.length){
          var condition = {$or : [{senderId : userId, receiverId : userObj[i]._id}, {receiverId : userId, senderId : userObj[i]._id}]};
          Friends.findFriends(1, condition, function(err, friendRes) {
            console.log('--------------------//', userObj[i]._id, friendRes);
            if(!err){
              var relationStatus = 0;
              var source = "dropp";
              if(friendRes){
                if(friendRes.relationStatus == 3) {
                  relationStatus = 1;
                }
                if(friendRes.relationStatus == 1 && String(friendRes.senderId) == String(userObj[i]._id)){
                  relationStatus = 2
                }
                if(friendRes.relationStatus == 1 && friendRes.senderId == userId){
                  relationStatus = 3;
                }
                if(friendRes.source){
                  source  = friendRes.source
                }
              }
              console.log(userObj[i].bio)
                mutualUserArray.push({
                  "_id"           : userObj[i]._id,
                  "fbId"          : userObj[i].fbId,
                  "firstName"     : userObj[i].firstName,
                  "lastName"      : userObj[i].lastName,
                  "profilePic"    : userObj[i].profilePic,
                  "email"         : userObj[i].email,
                  "bio"           : (userObj[i].bio) ? userObj[i].bio : "",
                  "postCount"     : 0,
                  "relationStatus": relationStatus,
                  "source" : source
                })
            }
            console.log('userObj.length', mutualUserArray);
            i = i+1;
            checkRelationStatus(i, userId, userObj, mutualUserArray, cb);
      });
    }else {
      //console.log(userObj.length, mutualUserArray);
      cb(mutualUserArray);
    }
}
//get friend list of my friend and friend requests or not 05/03/2018
var getFriendList = function(type, userId, page, searchText) {
  return new Promise(function(resolve) {
    var populateName = 'senderId';
    var condition = {receiverId : Common.toObjectId(userId), relationStatus: 1};
    if(type == 'myFriend'){

      var condition = {
        $or : [{senderId : Common.toObjectId(userId), relationStatus: 3}, {receiverId : Common.toObjectId(userId), relationStatus: 3}]
      };
      var relationStatus = 1;
    }else if (type == 'friendRequest') {
      var condition = {receiverId : Common.toObjectId(userId), relationStatus: 1};
      var relationStatus = 2;
    }
    var mutualUser = [];
    console.log('condition',condition)
    Friends.aggreateQuery(condition, page, searchText, Common.toObjectId(userId), function(err, friendRes) {
      console.log(err, friendRes);
      if(!err){
        if(friendRes){
          for (var i = 0; i < friendRes.length; i++) {
            if(friendRes[i].senderId._id == userId)
            {
                populateName = 'receiverId';
            }else {
                populateName = 'senderId';
            }
            mutualUser.push({
              "_id"           : friendRes[i][populateName]._id,
              "fbId"          : friendRes[i][populateName].fbId,
              "firstName"     : friendRes[i][populateName].firstName,
              "lastName"      : friendRes[i][populateName].lastName,
              "profilePic"    : friendRes[i][populateName].profilePic,
              "email"         : friendRes[i][populateName].email,
              "bio"           : (friendRes[i][populateName].bio)?friendRes[i][populateName].bio: "",
              "postCount"     : 0,
              "relationStatus": relationStatus,
              "source" : friendRes[i].source
            });
          }
        }
      }
      resolve(mutualUser);
  });
  });
}

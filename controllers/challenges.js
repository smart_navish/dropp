'use-strict'
var Users = require('./../services/users');
var Challenges = require('./../services/challenges');
var Noti = require('./../services/notifications');
var Friends = require('./../services/friends');
var Boom = require('boom');
var Common = require('./../config/common');
var userFields = Common.userFields;
var ffmpeg = require('ffmpeg');
/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 09.03.2018 / Navish
/****************************************************/

/* create chanllenge of end user 09.03.2018 By Navish */

exports.createChallenge = async function (request, reply) {
    console.log('In createChallenge');
    var userId = Common.getUserId(request.headers.authorization);
    var payloadData = request.payload;
    var chalInfo = {};
    var tagUsers = [];
    var hasTags = [];
    var notificationsData = [];
    chalInfo.userId       = userId;
    chalInfo.videoLink    = payloadData.videoLink;
    chalInfo.description  = payloadData.description;
    chalInfo.endDateTime  = new Date(payloadData.endDateTime);
    chalInfo.createdAt = new Date();
    if(payloadData.tagUsers.length > 0){
      for (var i = 0; i < payloadData.tagUsers.length; i++) {
        tagUsers.push({
          userId : payloadData.tagUsers[i],
          isAccepted : 0
        });
      }
    }
    if(payloadData.hashTag.length > 0){
      for (var k = 0; k < payloadData.hashTag.length; k++) {
        await Challenges.saveHash(payloadData.hashTag[k]);
      }
    }


    chalInfo.hashTag = payloadData.hashTag;
    chalInfo.tagUsers = tagUsers;
      Challenges.createChallenge(chalInfo, async function (err, chalObj) {
          if(!err)
          {
            for (var j = 0; j < payloadData.tagUsers.length; j++) {
              notificationsData.push({
                senderId : userId,
                receiverId : payloadData.tagUsers[j],
                challengeId : chalObj._id
              })
            }
            sendToMultiple(0, 3, notificationsData, function(response) {
              chalObj = Common.customeResponse(['createdAt', 'updatedAt','__v'], chalObj)
              reply(Common.successResponse('Challenge has been created successfully!', chalObj));
            });
          }
          else
          {
              return reply(Boom.badImplementation(err));
          }
      });
};


/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 30.07.2018 / Navish
/****************************************************/

/* update chanllenge of end user 30.07.2018 By Navish */

exports.addMoreTagUsers = async function (request, reply) {
    console.log('In updateChallenge');
    var userId = Common.getUserId(request.headers.authorization);
    var payloadData = request.payload;
    var tagUsers = [];
    var notificationsData = [];
    var query = {"_id": payloadData.challengeId};
    //console.log(payloadData);
    var filterTagUser = await Challenges.getDifferentTagUser(query, payloadData.tagUsers);

    if(filterTagUser.length > 0){
      for (var i = 0; i < filterTagUser.length; i++) {
        tagUsers.push({
          userId : filterTagUser[i],
          isAccepted : 0
        });
      }
    }
    var addToSetQuery = {tagUsers : { $each:tagUsers}};

      Challenges.addToSet(query, addToSetQuery, async function (err, chalObj) {
          if(!err)
          {
            for (var j = 0; j < filterTagUser.length; j++) {
              notificationsData.push({
                senderId : userId,
                receiverId : filterTagUser[j],
                challengeId : chalObj._id
              })
            }
            sendToMultiple(0, 3, notificationsData, function(response) {
              chalObj = Common.customeResponse(['createdAt', 'updatedAt','__v'], chalObj)
              reply(Common.successResponse('Users tagged successfully!', chalObj));
            });
          }
          else
          {
              return reply(Boom.badImplementation(err));
          }
      });
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 12.03.2018 / Navish
/****************************************************/

/* get One challenge for end user 12.03.2018 By Navish */
exports.challengeDetail = function(request, reply) {
  var userId = Common.getUserId(request.headers.authorization);
  var challengeId = request.params.challengeId;
  var comments = [];
  var tagUsers =[];
  var isChallengeAccepted = 0;
  var isTagged = false;
  Challenges.db.findOne({'_id': challengeId}).populate('comments.userId', userFields).populate('tagUsers.userId', userFields).populate('userId', userFields).exec(async function(err, chalObj){
      if(!err){
        console.log(chalObj)
        await Noti.readNotification(3, userId, challengeId);
        if(chalObj){
                var result = chalObj.comments.sort(function(a, b) {
                  return parseFloat((b.dateTime).getTime()) - parseFloat((a.dateTime).getTime());
                });
                console.log('result', JSON.stringify(result));
                var chalObjCommentlength = (chalObj.comments.length > 2) ? 2 : chalObj.comments.length
                for (var i = 0; i < chalObjCommentlength; i++) {
                  var commentObj = {};
                  console.log('i valuess', i)
                  commentObj.commentId = result[i]._id;
                  commentObj.commentPersonId  = result[i].userId._id;
                  commentObj.commentPersonName  = result[i].userId.firstName+' '+result[i].userId.lastName;
                  commentObj.commentPersonProfilePic = result[i].userId.profilePic;
                  commentObj.commentMessage = result[i].commentMessage;
                  commentObj.dateTime = result[i].dateTime.getTime();
                  commentObj.videoLink = result[i].videoLink;
                  comments.push(commentObj);
                }
               for (var j = 0; j < chalObj.tagUsers.length; j++) {
                 var tagObj = {};
                 if(chalObj.tagUsers[j].userId._id == userId){
                    isChallengeAccepted = chalObj.tagUsers[j].isAccepted;
                    isTagged = true;
                 }
                 tagObj.userId = chalObj.tagUsers[j].userId._id;
                 tagObj.userName = chalObj.tagUsers[j].userId.firstName+' '+chalObj.tagUsers[j].userId.lastName;
                 tagObj.userProfilePic = chalObj.tagUsers[j].userId.profilePic;
                 tagObj.isAccepted = chalObj.isAccepted;
                 tagUsers.push(tagObj);
               }
               if(new Date() - chalObj.endDateTime > 0) isChallengeAccepted = 3;
               var dataToSend = {
                  isChallengeAccepted : isChallengeAccepted,
                  videoLink : chalObj.videoLink,
                  description : chalObj.description,
                  createdTime : chalObj.createdAt.getTime(),
                  endDateTime : chalObj.endDateTime.getTime(),
                  challegeStatus: isChallengeAccepted,
                  isOwner: (chalObj.userId._id == userId)? true : false,
                  isTagged:isTagged,
                  creatorUserId: chalObj.userId._id,
                  creatorFirstName: chalObj.userId.firstName,
                  creatorLastName: chalObj.userId.lastName,
                  creatorProfilePic :chalObj.userId.profilePic,
                  tagUsers : tagUsers,
                  hashTag : chalObj.hashTag,
                  comments : comments,
                  totalCommentCount : chalObj.comments.length
               }
               reply(Common.successResponse('Successfully fetched!', dataToSend));
        }else {
          reply(Common.successResponse('Successfully fetched!', chalObj));
        }
      }else {
        return reply(Boom.badImplementation(err));
      }
  });
}

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 12.03.2018 / Navish
/****************************************************/

/* Accept/Reject chanllenge for end user 12.03.2018 By Navish */
exports.acceptRejectChallenge = function (request, reply) {
    var userId = Common.getUserId(request.headers.authorization);
    var payloadData = request.payload;
    var chalInfo = {};

    var challengeId = payloadData.challengeId;
    chalInfo.isAccepted = payloadData.isAccepted;

    //languages.create({name: "abc"});
    console.log({'_id': challengeId, 'tagUsers.userId':userId},{'tagUsers.$.isAccepted': chalInfo.isAccepted});
    Challenges.updateChallenge({'_id': challengeId, 'tagUsers.userId':userId},{'tagUsers.$.isAccepted': chalInfo.isAccepted}, {new: true}, async function(err, chalObj) {
      //console.log('---------------------->>>>',err, chalObj);

        if(!err){
            if(chalInfo.isAccepted == 1){
              var notificationsData = {
                senderId : userId,
                receiverId : chalObj.userId,
                challengeId : challengeId
              }
              var notiresponse = await Noti.sendPush(5, notificationsData);
            }
            var condition = {$or : [{senderId : userId, receiverId : chalObj.userId}, {receiverId : userId, senderId : chalObj.userId}]};
            var dataToEnter = {
              senderId : chalObj.userId,
              receiverId : userId,
              relationStatus : 3,
              source : 'dropp',
              createdAt : new Date()
            }
            console.log(condition, dataToEnter);
            Friends.updateFriend(condition, dataToEnter, {upsert: true}, function(upErr, upResult) {
              console.log(upErr, upResult);
              chalObj = Common.customeResponse(['createdAt', 'updatedAt', '__v'], chalObj)
              reply(Common.successResponse('Challenge has been '+chalInfo.isAccepted +' successfully!', chalObj));
            })

        }
        else
        {
            return reply(Boom.badImplementation(err));
        }
    });
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 12.03.2018 / Navish
/****************************************************/

/* Comment on challenges for end user 12.03.2018 By Navish */
exports.commentOnChallenge = function (request, reply) {
    var userId = Common.getUserId(request.headers.authorization);
    var payloadData = request.payload;
    console.log('payloadData', JSON.stringify(payloadData));
    var chalInfo = {};
    var challengeId = payloadData.challengeId;
    chalInfo.userId = userId;
    //chalInfo.commentMessage = payloadData.commentMessage;
    //chalInfo.userTagged = payloadData.userTagged;
    chalInfo.commentMessage = "";
    chalInfo.userTagged = [];
    chalInfo.dateTime = new Date();
    chalInfo.videoLink = payloadData.videoLink;

    Challenges.addToSet({'_id': challengeId},{'comments': chalInfo}, {new: true}, async function(err, userObj) {
        if(!err){
            if(userId != userObj.userId){
              var notificationsData = {
                senderId : userId,
                receiverId : userObj.userId,
                challengeId : challengeId
              }
              var notiresponse = await Noti.sendPush(4, notificationsData);
            }

            // if(payloadData.userTagged.length > 0){
            //   for (var j = 0; j < payloadData.userTagged.length; j++) {
            //     notificationsData.push({
            //       senderId : userId,
            //       receiverId : payloadData.userTagged[j],
            //       challengeId : userObj._id
            //     })
            //   }
            //   sendToMultiple(0, 6, notificationsData, function(response) {
            //     userObj = Common.customeResponse(['createdAt', 'updatedAt', '__v'], userObj)
            //     var result = userObj.comments.filter(comments => (comments.commentMessage == chalInfo.commentMessage && comments.userId == userId))[0];
            //     reply(Common.successResponse('Successfully commented!', result));
            //   });
            // }
            // else {
              userObj = Common.customeResponse(['createdAt', 'updatedAt', '__v'], userObj)
              var result = userObj.comments.filter(comments => (comments.commentMessage == chalInfo.commentMessage && comments.userId == userId))[0];
              reply(Common.successResponse('Successfully commented!', result));
            //}
        }
        else
        {
            return reply(Boom.badImplementation(err));
        }
    });
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 12.03.2018 / Navish
/****************************************************/

/* Get comment for end user 12.03.2018 By Navish */
exports.commentListOfChallenge = function(request, reply) {
  var userId = Common.getUserId(request.headers.authorization);
  var challengeId = request.params.challengeId;
  var comments = [];
  Challenges.db.findOne({'_id': challengeId}).populate('comments.userId', userFields).exec(function(err, chalObj){
      if(!err){
        if(chalObj){
            var result = chalObj.comments.sort(function(a, b) {
              return parseFloat((b.dateTime).getTime()) - parseFloat((a.dateTime).getTime());
            });
            //console.log(result);
                for (var i = 0; i < result.length; i++) {
                  var commentObj = {};
                  commentObj.commentId = result[i]._id;
                  commentObj.commentPersonId  = result[i].userId._id;
                  commentObj.commentPersonName  = result[i].userId.firstName+' '+result[i].userId.lastName;
                  commentObj.commentPersonProfilePic = result[i].userId.profilePic;
                  commentObj.commentMessage = result[i].commentMessage;
                  commentObj.dateTime = result[i].dateTime.getTime();
                  commentObj.videoLink = result[i].videoLink;
                  comments.push(commentObj);
                }
               var dataToSend = {
                  commentList : comments,
                  totalCommentCount : result.length
               }
               reply(Common.successResponse('Successfully fetched!', dataToSend));
        }else {
          reply(Common.successResponse('Successfully fetched!', chalObj));
        }
      }else {
        return reply(Boom.badImplementation(err));
      }
  });
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 12.03.2018 / Navish
/****************************************************/

/* Get list of challeges for end user 12.03.2018 By Navish */
exports.getListOfChallenge = async function(request, reply) {
  var userId = Common.getUserId(request.headers.authorization);
  var type = request.params.type;
  var page = request.params.page;
  var searchType = request.params.searchType;
  var activeCountValue = request.params.activeCountValue;
  var typeoffetch = null;
  console.log('request.params', request.params);
  var searchText = request.query.searchQuery?request.query.searchQuery:"";
  var matchObj = {$or: [{'tagUsers.userId': Common.toObjectId(userId)}, {'userId': Common.toObjectId(userId)}]};
  var challegesList = [];
  if(type == 'myCreatedChallenges'){
    matchObj.userId = request.query.userId?Common.toObjectId(request.query.userId): Common.toObjectId(userId);
  }else if(type == 'myAcceptedChallenges'){
    matchObj = {'tagUsers.userId': Common.toObjectId(userId), 'tagUsers.isAccepted': 1};
  }else if(type == 'myTagedChallenges'){
    matchObj = {'tagUsers.userId': Common.toObjectId(userId)};
  }else if (type == 'allChallenges') { //Challenges related to friends whether user is tagged or not expect user's onwn created challenges.
    var friendIds = await Friends.friendIds(userId);
    // matchObj = {'userId': {$ne : Common.toObjectId(userId)}, $or : [{'tagUsers.userId': {$in : friendIds}}, {'userId': {$in : friendIds}}]};
    matchObj = {$or : [{'tagUsers.userId': {$in : friendIds}}, {'userId': {$in : friendIds}}, {'userId': Common.toObjectId(userId)}]};
  }else if (type == 'globalChallenges') { //All challenges on Dropp platform
    matchObj = {};
  }
  //matchObj.endDateTime = {$gte : new Date()}
  var responseData = {};

  responseData.totalListSize = await Challenges.challengeCount(matchObj, searchText, searchType);
  responseData.activeCountValue = activeCountValue;
  console.log('responseData.totalListSize',responseData.totalListSize);
  matchObj.endDateTime = {$gte : new Date()};
  var activeCount = await Challenges.challengeCount(matchObj, searchText, searchType);
  console.log('page ', page * 10, 'activeCount ', activeCount);
  if(activeCount < page * 10){
      matchObj.endDateTime = {$lt : new Date()};
      var expireCount = await Challenges.challengeCount(matchObj, searchText, searchType);
      if(activeCountValue == 0){
        responseData.activeCountValue = page;
      }
      page = page - activeCountValue;
      typeoffetch = 'expired'
  }
  console.log('activeCountValue >> ', activeCountValue, 'expireCount', expireCount);
  Challenges.aggreateQuery(matchObj, searchText, page, searchType, typeoffetch, function(err, chalObj) {
      if(!err){
          if(chalObj.length > 0){
              for (var i = 0; i < chalObj.length; i++) {
                var challengeObj = {
                  challegeStatus : 0,
                  isOwner : false,
                  isTagged : false
                };
                challengeObj._id = chalObj[i]._id;
                challengeObj.creatorUserId = chalObj[i].user._id;
                challengeObj.creatorFirstName = chalObj[i].user.firstName;
                challengeObj.creatorLastName = chalObj[i].user.lastName;
                challengeObj.creatorProfilePic = chalObj[i].user.profilePic;
                challengeObj.videoLink = chalObj[i].videoLink;
                challengeObj.description = chalObj[i].description;
                challengeObj.startDateTime = chalObj[i].createdAt.getTime();
                challengeObj.endDateTime = chalObj[i].endDateTime.getTime();
                challengeObj.hashTag = chalObj[i].hashTag;
                for (var j = 0; j < chalObj[i].tagUsers.length; j++) {
                  if(chalObj[i].tagUsers[j].userId == userId){
                    challengeObj.challegeStatus = chalObj[i].tagUsers[j].isAccepted;
                    challengeObj.isTagged = true;
                  }
                }

                if(chalObj[i].user._id == userId){
                  challengeObj.isOwner = true;
                }

                if(new Date() - challengeObj.endDateTime > 0) challengeObj.challegeStatus = 3;
                challegesList.push(challengeObj);
              }

          }
          responseData.challengeList = challegesList;
          reply(Common.successResponse('Successfully fetched!', responseData));

      }else {
        return reply(Boom.badImplementation(err));
      }
  })
};

/****************************************************/
// Filename: user.js
// Created: Navis Kumar
// Change history:
// 02.08.2018 / Navish
/****************************************************/

/* Get list of hashtags for end user 08.08.2018 By Navish */
exports.getHashTags = async function(request, reply) {
  var userId = Common.getUserId(request.headers.authorization);
  var page = request.params.page;
  var searchText = request.query.searchQuery?request.query.searchQuery:"";
  var data = await Challenges.getHashTags(searchText, page);
  var responseData = {};
  responseData.totalListSize = await Challenges.getHashTags(searchText);
  responseData.hashTag = data;
  reply(Common.successResponse('Successfully fetched!', responseData, null));
}


//Testing apis
exports.ffmpeg = function() {
    try {
	var process = new ffmpeg('/home/navish/projects/dropp/public/samp.mp4');
  	process.then(function (video) {
  		// Callback mode
    console.log('in file');
      		video.fnExtractFrameToJPG('/home/navish/projects', {
      			frame_rate : 1,
      			number : 1,
            size : '320x240',
      			file_name : 'sdasdf'
      		}, function (error, files) {
            console.log('in function', error);
      			if (!error)
      				console.log('Frames: ' + files);
      		});
      	}, function (err) {
      		console.log('Error: ' + err);
      	});
      } catch (e) {
      	console.log(e.code);
      	console.log(e.msg);
      }
}

//send notificatons to multiple
var sendToMultiple = async function(i, type, notificationsData, callback) {
  if(notificationsData.length > i){
      var notiresponse = await Noti.sendPush(type, notificationsData[i]);
      i = i+1;
      sendToMultiple(i, type, notificationsData, callback);
      console.log('I value and notification', notiresponse);
  }else {
    callback('done');
  }
}

'use-strict';

//Friend model for friend module
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var friendSchema = new Schema({
    senderId : {type: Schema.ObjectId, ref: 'users'},
    receiverId : {type: Schema.ObjectId, ref: 'users'},
    relationStatus : {type: Number, default: 1}, //1- Friend request send, 3- Friend
    source : {type : String, enum : ['facebook', 'dropp'], default: 'dropp'},
    createdAt   : {
      type      : Date,
      default   : Date.now()
    },
    updatedAt   :  {
      type      : Date,
      default   : Date.now()
    }
})

module.exports = mongoose.model('friends', friendSchema);

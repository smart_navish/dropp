'use-strict';

//Friend model for friend module
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var notiSchema = new Schema({
    notificationType : {type: Number, default: 0},  // 0-None, 1-Friends Request, 2- Accept Friend, 3-challenge tag,4- challenge comment
    senderId : {type: Schema.ObjectId, ref: 'users'},
    receiverId : {type: Schema.ObjectId, ref: 'users'},
    challengeId : {type: Schema.ObjectId, default: null},
    notificationBody: String,
    isRead : {type: Boolean, default: false},
    createdAt   : {
      type      : Date,
      default   : Date.now()
    },
    updatedAt   :  {
      type      : Date,
      default   : Date.now()
    }
})

module.exports = mongoose.model('notifications', notiSchema);

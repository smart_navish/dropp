module.exports = {
  users : require('./users'),
  friends : require('./friends'),
  challenges : require('./challenges'),
  notifications : require('./notifications'),
  hashtags : require('./hashtags')
}

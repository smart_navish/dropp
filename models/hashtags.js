'use-strict';

//Friend model for friend module
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var hashtagSchema = new Schema({
    hashName : {type: String},
    totalChallenges : {type: Number, default: 0},
    createdAt   : {
      type      : Date,
      default   : Date.now()
    },
    updatedAt   :  {
      type      : Date,
      default   : Date.now()
    }
})

module.exports = mongoose.model('hashtags', hashtagSchema);

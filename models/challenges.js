'use strict';
/**
 * Challenges Model
 **/
 var mongoose = require('mongoose'),
 Schema = mongoose.Schema;

 var comments = {
    userId: {type:  Schema.ObjectId, ref: 'users'},
    commentMessage: String,
    dateTime : Date,
    userTagged : [{type:  Schema.ObjectId, ref: 'users'}],
    videoLink: String
 }

 var tagUsers = {
    userId: {type:  Schema.ObjectId, ref: 'users'},
    isAccepted: {type: Number, default: 0} // 0 = No Action, 1 = Accepted, 2 = Rejected, 3 = Expired, 4 = Completed
 }

 var challengesSchema = new Schema({
  userId         : {type:  Schema.ObjectId, ref: 'users'},
  videoLink      : String,
  description    : String,
  endDateTime    : Date,   //In milisecond
  tagUsers       : [tagUsers],
  hashTag        : [],
  comments       : [comments],
  createdAt   : {
    type      : Date,
    default   : Date.now()
  },
  updatedAt   :  {
    type      : Date,
    default   : Date.now()
  }
});


module.exports = mongoose.model('challenges', challengesSchema);
